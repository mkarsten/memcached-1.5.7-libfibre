/* -*- Mode: C; tab-width: 4; c-basic-offset: 4; indent-tabs-mode: nil -*- */
/*
 * Thread management for memcached.
 */
#include "memcached.h"
#ifdef EXTSTORE
#include "storage.h"
#endif
#include <assert.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#ifdef __sun
#include <atomic.h>
#endif

typedef struct conn_queue_item CQ_ITEM;
struct conn_queue_item {
    int               sfd;
    enum conn_states  init_state;
    int               read_buffer_size;
    enum network_transport     transport;
    conn *c;
};

/* Locks for cache LRU operations */
uthread_mutex_t lru_locks[POWER_LARGEST];

/* Connection lock around accepting new connections */
uthread_mutex_t conn_lock;

#if !defined(HAVE_GCC_ATOMICS) && !defined(__sun)
uthread_mutex_t atomics_mutex;
#endif

/* Lock for global stats */
static uthread_mutex_t stats_lock;

static uthread_mutex_t *item_locks;
/* size of the item lock hash table */
static uint32_t item_lock_count;
unsigned int item_lock_hashpower;
#define hashsize(n) ((unsigned long int)1<<(n))
#define hashmask(n) (hashsize(n)-1)

static SP_THREAD* threads = NULL;

// define thread-local
__thread SP_THREAD* currthread;

/* item_lock() must be held for an item before any modifications to either its
 * associated hash bucket, or the structure itself.
 * LRU modifications must hold the item lock, and the LRU lock.
 * LRU's accessing items must item_trylock() before modifying an item.
 * Items accessible from an LRU must not be freed or modified
 * without first locking and removing from the LRU.
 */

void item_lock(uint32_t hv) {
    mutex_lock(&item_locks[hv & hashmask(item_lock_hashpower)]);
}

void *item_trylock(uint32_t hv) {
    uthread_mutex_t *lock = &item_locks[hv & hashmask(item_lock_hashpower)];
    if (uthread_mutex_trylock(lock) == 0) {
        return lock;
    }
    return NULL;
}

void item_trylock_unlock(void *lock) {
    mutex_unlock((uthread_mutex_t *) lock);
}

void item_unlock(uint32_t hv) {
    mutex_unlock(&item_locks[hv & hashmask(item_lock_hashpower)]);
}

/*
 * Sets whether or not we accept new connections.
 */
void accept_new_conns(const bool do_accept) {
    uthread_mutex_lock(&conn_lock);
    do_accept_new_conns(do_accept);
    uthread_mutex_unlock(&conn_lock);
}
/****************************** LIBEVENT THREADS *****************************/

/*
 * Set up a thread's information.
 */
static void setup_thread(SP_THREAD *me) {
    if (uthread_mutex_init(&me->stats.mutex, NULL) != 0) {
        perror("Failed to initialize mutex");
        exit(EXIT_FAILURE);
    }

    me->suffix_cache = cache_create("suffix", SUFFIX_SIZE, sizeof(char*),
                                    NULL, NULL);
    if (me->suffix_cache == NULL) {
        fprintf(stderr, "Failed to create suffix cache\n");
        exit(EXIT_FAILURE);
    }
#ifdef EXTSTORE
    me->io_cache = cache_create("io", sizeof(io_wrap), sizeof(char*), NULL, NULL);
    if (me->io_cache == NULL) {
        fprintf(stderr, "Failed to create IO object cache\n");
        exit(EXIT_FAILURE);
    }
#endif
}

static void setup_thread2(SP_THREAD *me) {
    currthread = me;
#if TESTING_AFFINITY
    cpu_set_t onecpu;
    CPU_ZERO(&onecpu);
    CPU_SET(me->cpu_index, &onecpu);
    pthread_setaffinity_np(pthread_self(), sizeof(onecpu), &onecpu);
#endif
    me->l = logger_create();
    me->lru_bump_buf = item_lru_bump_buf_create();
    if (me->l == NULL || me->lru_bump_buf == NULL) {
        abort();
    }
}

static void setup_thread3(void *me) {
    setup_thread2(me);
    if (settings.drop_privileges) {
        drop_worker_privileges();
    }
}

typedef struct _cond_stack_t cond_stack_t;
struct _cond_stack_t {
    uthread_cond_t cond;
    CQ_ITEM* item;
    cond_stack_t* next;
};

static uthread_mutex_t thread_pool_lock;
cond_stack_t*          thread_pool_stack = NULL;

/*
 * Worker thread: main event loop
 */
static void *worker_libevent(void *arg) {
    cond_stack_t work;
    work.item = arg;
    uthread_cond_init(&work.cond, NULL);

    for (;;) {
        conn* c = conn_new(work.item->sfd, work.item->init_state, work.item->read_buffer_size, work.item->transport);
        if (c == NULL) {
            if (IS_UDP(work.item->transport)) {
                fprintf(stderr, "Can't listen for events on UDP socket\n");
                exit(1);
            } else {
                if (settings.verbose > 0) {
                    fprintf(stderr, "Can't listen for events on fd %d\n", work.item->sfd);
                }
                uthread_close(work.item->sfd);
            }
        }
        free(work.item);
        work.item = NULL;

        conn_handler(c);

        uthread_mutex_lock(&thread_pool_lock);
        work.next = thread_pool_stack;
        thread_pool_stack = &work;
        // could use timedwait for timing out unsused worker threads?
        uthread_cond_wait(&work.cond, &thread_pool_lock);
        uthread_mutex_unlock(&thread_pool_lock);

    }
    return NULL;
}

static uthread_cluster_t* clusters;
static int num_clusters = 1;
static int cidx = 0;
/*
 * Dispatches a new connection to another thread. This is only ever called
 * from the main thread, either during initialization (for UDP) or because
 * of an incoming connection.
 */
void dispatch_conn_new(int sfd, enum conn_states init_state,
                       int read_buffer_size, enum network_transport transport) {
    CQ_ITEM *item = calloc(1, sizeof(CQ_ITEM));
    if (item == NULL) {
        uthread_close(sfd);
        /* given that malloc failed this may also fail, but let's try */
        fprintf(stderr, "Failed to allocate memory for connection object\n");
        return ;
    }

    item->sfd = sfd;
    item->init_state = init_state;
    item->read_buffer_size = read_buffer_size;
    item->transport = transport;

    /* Create threads after we've done all the libevent setup. */
    uthread_mutex_lock(&thread_pool_lock);

    if (thread_pool_stack == NULL) { // create new worker thread
        int ret;
        uthread_t pt;
        cidx = (cidx + 1) % num_clusters;
        uthread_mutex_unlock(&thread_pool_lock);
        uthread_attr_t attr;
        uthread_attr_init(&attr);
        uthread_attr_setcluster(&attr, clusters[cidx]);
        if ((ret = uthread_create(&pt, &attr, worker_libevent, item)) != 0) {
            fprintf(stderr, "Can't create thread: %s\n", strerror(ret));
            while(true);
            exit(1);
        }
        uthread_attr_destroy(&attr);
    } else { // get worker thread from pool stack
        cond_stack_t* work = thread_pool_stack;
        thread_pool_stack = thread_pool_stack->next;
        uthread_mutex_unlock(&thread_pool_lock); // unlock early, since...
        work->item = item;
        uthread_cond_signal(&work->cond);        // ...since cond is private
    }

    MEMCACHED_CONN_DISPATCH(sfd, thread->thread_id);
}

/********************************* ITEM ACCESS *******************************/

/*
 * Allocates a new item.
 */
item *item_alloc(char *key, size_t nkey, int flags, rel_time_t exptime, int nbytes) {
    item *it;
    /* do_item_alloc handles its own locks */
    it = do_item_alloc(key, nkey, flags, exptime, nbytes);
    return it;
}

/*
 * Returns an item if it hasn't been marked as expired,
 * lazy-expiring as needed.
 */
item *item_get(const char *key, const size_t nkey, conn *c, const bool do_update) {
    item *it;
    uint32_t hv;
    hv = hash(key, nkey);
    item_lock(hv);
    it = do_item_get(key, nkey, hv, c, do_update);
    item_unlock(hv);
    return it;
}

item *item_touch(const char *key, size_t nkey, uint32_t exptime, conn *c) {
    item *it;
    uint32_t hv;
    hv = hash(key, nkey);
    item_lock(hv);
    it = do_item_touch(key, nkey, exptime, hv, c);
    item_unlock(hv);
    return it;
}

/*
 * Links an item into the LRU and hashtable.
 */
int item_link(item *item) {
    int ret;
    uint32_t hv;

    hv = hash(ITEM_key(item), item->nkey);
    item_lock(hv);
    ret = do_item_link(item, hv);
    item_unlock(hv);
    return ret;
}

/*
 * Decrements the reference count on an item and adds it to the freelist if
 * needed.
 */
void item_remove(item *item) {
    uint32_t hv;
    hv = hash(ITEM_key(item), item->nkey);

    item_lock(hv);
    do_item_remove(item);
    item_unlock(hv);
}

/*
 * Replaces one item with another in the hashtable.
 * Unprotected by a mutex lock since the core server does not require
 * it to be thread-safe.
 */
int item_replace(item *old_it, item *new_it, const uint32_t hv) {
    return do_item_replace(old_it, new_it, hv);
}

/*
 * Unlinks an item from the LRU and hashtable.
 */
void item_unlink(item *item) {
    uint32_t hv;
    hv = hash(ITEM_key(item), item->nkey);
    item_lock(hv);
    do_item_unlink(item, hv);
    item_unlock(hv);
}

/*
 * Does arithmetic on a numeric item value.
 */
enum delta_result_type add_delta(conn *c, const char *key,
                                 const size_t nkey, bool incr,
                                 const int64_t delta, char *buf,
                                 uint64_t *cas) {
    enum delta_result_type ret;
    uint32_t hv;

    hv = hash(key, nkey);
    item_lock(hv);
    ret = do_add_delta(c, key, nkey, incr, delta, buf, cas, hv);
    item_unlock(hv);
    return ret;
}

/*
 * Stores an item in the cache (high level, obeys set/add/replace semantics)
 */
enum store_item_type store_item(item *item, int comm, conn* c) {
    enum store_item_type ret;
    uint32_t hv;

    hv = hash(ITEM_key(item), item->nkey);
    item_lock(hv);
    ret = do_store_item(item, comm, c, hv);
    item_unlock(hv);
    return ret;
}

/******************************* GLOBAL STATS ******************************/

void STATS_LOCK() {
    uthread_mutex_lock(&stats_lock);
}

void STATS_UNLOCK() {
    uthread_mutex_unlock(&stats_lock);
}

void threadlocal_stats_reset(void) {
#if 0
    for (int ii = 0; ii < settings.num_threads; ++ii) {
        uthread_mutex_lock(&threads[ii].stats.mutex);
#define X(name) threads[ii].stats.name = 0;
        THREAD_STATS_FIELDS
#ifdef EXTSTORE
        EXTSTORE_THREAD_STATS_FIELDS
#endif
#undef X

        memset(&threads[ii].stats.slab_stats, 0,
                sizeof(threads[ii].stats.slab_stats));
        memset(&threads[ii].stats.lru_hits, 0,
                sizeof(uint64_t) * POWER_LARGEST);

        uthread_mutex_unlock(&threads[ii].stats.mutex);
    }
#endif
}

void threadlocal_stats_aggregate(struct thread_stats *stats) {
    /* The struct has a mutex, but we can safely set the whole thing
     * to zero since it is unused when aggregating. */
    memset(stats, 0, sizeof(*stats));

    for (int ii = 0; ii < settings.num_threads; ++ii) {
        uthread_mutex_lock(&threads[ii].stats.mutex);
#define X(name) stats->name += threads[ii].stats.name;
        THREAD_STATS_FIELDS
#ifdef EXTSTORE
        EXTSTORE_THREAD_STATS_FIELDS
#endif
#undef X

        for (int sid = 0; sid < MAX_NUMBER_OF_SLAB_CLASSES; sid++) {
#define X(name) stats->slab_stats[sid].name += \
            threads[ii].stats.slab_stats[sid].name;
            SLAB_STATS_FIELDS
#undef X
        }

        for (int sid = 0; sid < POWER_LARGEST; sid++) {
            stats->lru_hits[sid] +=
                threads[ii].stats.lru_hits[sid];
            stats->slab_stats[CLEAR_LRU(sid)].get_hits +=
                threads[ii].stats.lru_hits[sid];
        }

        uthread_mutex_unlock(&threads[ii].stats.mutex);
    }
}

void slab_stats_aggregate(struct thread_stats *stats, struct slab_stats *out) {
    memset(out, 0, sizeof(*out));

    for (int sid = 0; sid < MAX_NUMBER_OF_SLAB_CLASSES; sid++) {
#define X(name) out->name += stats->slab_stats[sid].name;
        SLAB_STATS_FIELDS
#undef X
    }
}

void memcached_thread_preinit() {
    uthread_mutex_init(&conn_lock, NULL);
#if !defined(HAVE_GCC_ATOMICS) && !defined(__sun)
    uthread_mutex_init(&atomics_mutex, NULL);
#endif
    uthread_mutex_init(&stats_lock, NULL);
    uthread_mutex_init(&thread_pool_lock, NULL);
}

/*
 * Initializes the thread subsystem, creating various worker threads.
 *
 * nthreads  Number of worker event handler threads to spawn
 */
void memcached_thread_init(int nthreads, int nclusters, void *arg) {
    int         i;
    int         power;

    for (i = 0; i < POWER_LARGEST; i++) {
        uthread_mutex_init(&lru_locks[i], NULL);
    }

    /* Want a wide lock table, but don't waste memory */
    if (nthreads < 3) {
        power = 10;
    } else if (nthreads < 4) {
        power = 11;
    } else if (nthreads < 5) {
        power = 12;
    } else if (nthreads <= 10) {
        power = 13;
    } else if (nthreads <= 20) {
        power = 14;
    } else {
        /* 32k buckets. just under the hashpower default. */
        power = 15;
    }

    if (power >= hashpower) {
        fprintf(stderr, "Hash table power size (%d) cannot be equal to or less than item lock table (%d)\n", hashpower, power);
        fprintf(stderr, "Item lock table grows with `-t N` (worker threadcount)\n");
        fprintf(stderr, "Hash table grows with `-o hashpower=N` \n");
        exit(1);
    }

    item_lock_count = hashsize(power);
    item_lock_hashpower = power;

    item_locks = calloc(item_lock_count, sizeof(uthread_mutex_t));
    if (! item_locks) {
        perror("Can't allocate item locks");
        exit(1);
    }
    for (i = 0; i < item_lock_count; i++) {
        uthread_mutex_init(&item_locks[i], NULL);
    }

    threads = calloc(nthreads, sizeof(SP_THREAD));
    if (!threads) {
        perror("Can't allocate thread descriptors");
        exit(1);
    }

#if TESTING_AFFINITY
    cpu_set_t allcpus;
    CPU_ZERO(&allcpus);
    pthread_getaffinity_np(pthread_self(), sizeof(allcpus), &allcpus);
    int cpu = 0;
    while (!CPU_ISSET(cpu, &allcpus)) cpu = (cpu + 1) % CPU_SETSIZE;
    threads[0].cpu_index = cpu;
    printf("affinity: %d", cpu);
#endif

    for (i = 0; i < nthreads; i++) {
#ifdef EXTSTORE
        threads[i].storage = arg;
#endif
        setup_thread(&threads[i]);
    }
    threads[0].thread_id = pthread_self();
    setup_thread2(&threads[0]);

    num_clusters = nclusters;
    clusters = malloc(sizeof(uthread_cluster_t) * num_clusters);
    clusters[0] = uthread_cluster_self();
    for (i = 1; i < num_clusters; i++) {
        if (uthread_cluster_create(&clusters[i]) < 0) {
            perror("Can't create clusters");
            exit(1);
        }
    }
    for (i = 1; i < nthreads; i++) {
#if TESTING_AFFINITY
        do { cpu = (cpu + 1) % CPU_SETSIZE; } while (!CPU_ISSET(cpu, &allcpus));
        threads[i].cpu_index = cpu;
        printf(" %d", cpu);
#endif
        if (uthread_add_worker(clusters[i % num_clusters], &threads[i].thread_id, setup_thread3, &threads[i]) < 0) {
            perror("Can't create workers");
            exit(1);
        }
    }
#if TESTING_AFFINITY
    printf("\n");
#endif
}
