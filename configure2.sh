#!/bin/sh
[ -z "$1" ] && {
  echo "usage: $0 <path to libfibre>"
  exit 1
}

BASEDIR=$(dirname "$0")

cd $1
PTLF=$(pwd)
cd -
shift

[ "$(uname)" = "Linux" ] && {
	SED=sed
	MAKE=make
	count=$(nproc)
	FLIBS=-lfibre
} || {
	SED=gsed
	MAKE=gmake
	count=$(sysctl kern.smp.cpus|cut -c16- || echo 1)
	FLIBS="-lfibre -lexecinfo"
}

$MAKE -j $count -C $PTLF lib

LDFLAGS="-L$PTLF/src -Wl,-rpath=$PTLF/src" LIBS="$FLIBS" \
CFLAGS="-I$PTLF/src -g -O2 -DUSE_LIBFIBRE=1" $BASEDIR/configure $*

$SED -i -e "s|-levent ||" Makefile
