#ifndef _uthread_h_
#define _uthread_h_ 1

#if USE_LIBFIBRE

#include "libfibre/cfibre.h"

#define USE_FASTMUTEX 1
#if USE_FASTMUTEX
#define uthread_mutex_t            cfibre_fastmutex_t
#define uthread_mutex_init         cfibre_fastmutex_init
#define uthread_mutex_destroy      cfibre_fastmutex_destroy
#define uthread_mutex_lock         cfibre_fastmutex_lock
#define uthread_mutex_trylock      cfibre_fastmutex_trylock
#define uthread_mutex_unlock       cfibre_fastmutex_unlock
#define uthread_cond_wait          cfibre_fastcond_wait
#define uthread_cond_timedwait     cfibre_fastcond_timedwait
#else
#define uthread_mutex_t            cfibre_mutex_t
#define uthread_mutex_init         cfibre_mutex_init
#define uthread_mutex_destroy      cfibre_mutex_destroy
#define uthread_mutex_lock         cfibre_mutex_lock
#define uthread_mutex_trylock      cfibre_mutex_trylock
#define uthread_mutex_unlock       cfibre_mutex_unlock
#define uthread_cond_wait          cfibre_cond_wait
#define uthread_cond_timedwait     cfibre_cond_timedwait
#endif // USE_FASTMUTEX

#define uthread_t                  cfibre_t
#define uthread_cond_t             cfibre_cond_t
#define uthread_attr_t             cfibre_attr_t
#define uthread_cluster_t          cfibre_cluster_t
#define uthread_init               cfibre_init
#define uthread_cluster_create     cfibre_cluster_create
#define uthread_cluster_self       cfibre_cluster_self
#define uthread_add_worker         cfibre_add_worker
#define uthread_pause              cfibre_pause
#define uthread_resume             cfibre_resume
#define uthread_get_errno          cfibre_get_errno
#define uthread_attr_init          cfibre_attr_init
#define uthread_attr_destroy       cfibre_attr_destroy
#define uthread_attr_setbackground cfibre_attr_setbackground
#define uthread_attr_setaffinity   cfibre_attr_setaffinity
#define uthread_attr_setpriority   cfibre_attr_setpriority
#define uthread_attr_setcluster    cfibre_attr_setcluster
#define uthread_create             cfibre_create
#define uthread_join               cfibre_join
#define uthread_yield              cfibre_yield
#define uthread_cond_init          cfibre_cond_init
#define uthread_cond_destroy       cfibre_cond_destroy
#define uthread_cond_signal        cfibre_cond_signal
#define uthread_socket             cfibre_socket
#define uthread_bind               cfibre_bind
#define uthread_listen             cfibre_listen
#define uthread_accept             cfibre_accept
#define uthread_connect            cfibre_connect
#define uthread_dup                cfibre_dup
#define uthread_close              cfibre_close
#define uthread_sendmsg            cfibre_sendmsg
#define uthread_write              cfibre_write
#define uthread_recvfrom           cfibre_recvfrom
#define uthread_read               cfibre_read
#define uthread_usleep             cfibre_usleep
#define uthread_sleep              cfibre_sleep

#elif USE_CFORALL

#include <pthread.h>
#include <cfa/concurrency/clib/cfathread.h>

static inline int noop(void) { return 0; }

#define uthread_mutex_t            cfathread_mutex_t
#define uthread_mutex_init         cfathread_mutex_init
#define uthread_mutex_destroy      cfathread_mutex_destroy
#define uthread_mutex_lock         cfathread_mutex_lock
#define uthread_mutex_trylock      cfathread_mutex_trylock
#define uthread_mutex_unlock       cfathread_mutex_unlock
#define uthread_cond_wait          cfathread_cond_wait
#define uthread_cond_timedwait     cfathread_cond_timedwait

#define uthread_t                  cfathread_t
#define uthread_cond_t             cfathread_cond_t
#define uthread_attr_t             cfathread_attr_t
#define uthread_cluster_t          cfathread_cluster_t
#define uthread_init               noop
#define uthread_cluster_create     cfathread_cluster_create
#define uthread_cluster_self       cfathread_cluster_self
#define uthread_add_worker         cfathread_cluster_add_worker
#define uthread_pause              cfathread_cluster_pause
#define uthread_resume             cfathread_cluster_resume
#define uthread_get_errno          cfathread_get_errno
#define uthread_attr_init          cfathread_attr_init
#define uthread_attr_destroy       cfathread_attr_destroy
#define uthread_attr_setbackground cfathread_attr_setbackground
#define uthread_attr_setaffinity   cfathread_attr_setaffinity
#define uthread_attr_setpriority   cfathread_attr_setpriority
#define uthread_attr_setcluster    cfathread_attr_setcluster
#define uthread_create             cfathread_create
#define uthread_join               cfathread_join
#define uthread_yield              cfathread_yield
#define uthread_cond_init          cfathread_cond_init
#define uthread_cond_destroy       cfathread_cond_destroy
#define uthread_cond_signal        cfathread_cond_signal
#define uthread_socket             cfathread_socket
#define uthread_bind               cfathread_bind
#define uthread_listen             cfathread_listen
#define uthread_accept             cfathread_accept
#define uthread_connect            cfathread_connect
#define uthread_dup                cfathread_dup
#define uthread_close              cfathread_close
#define uthread_sendmsg            cfathread_sendmsg
#define uthread_write              cfathread_write
#define uthread_recvfrom           cfathread_recvfrom
#define uthread_read               cfathread_read
#define uthread_usleep             cfathread_usleep
#define uthread_sleep              cfathread_sleep

#else

#error unsupported user-level threading runtime

#endif

#endif /* _uthread_h_ */
